import React from 'react';
import {
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from '@mui/material';

interface PropType {
  sx: {
    width: string;
  };
  value: string;
  placeholder: string;
  options: string[];
  handleChange: (event: SelectChangeEvent) => void;
}

const RPSelect = ({
  sx,
  value,
  placeholder,
  options,
  handleChange,
}: PropType) => {
  return (
    <div>
      <Select displayEmpty sx={sx} value={value} onChange={handleChange}>
        <MenuItem disabled value=''>
          <em>{placeholder}</em>
        </MenuItem>
        {options.map((el) => (
          <MenuItem key={`${el}-key`} value={el}>
            {el}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

export default RPSelect;
