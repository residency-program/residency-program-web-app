import React, { useState } from 'react';
import {
  Box,
  Button,
  TextField,
  Typography,
  Autocomplete,
  CircularProgress,
  Select,
  MenuItem,
} from '@mui/material';
import RPSelect from '@/components/RPSelect';
import { residencyProgramSearchStyles } from './residencyProgramSearchStyles';
import stateUtils, { UserOptionsType } from './utils/stateUtils';

const ResidencyProgramSearch = () => {
  const {
    mainContainer,
    userSelectStyle,
    subContainer,
    headerStyles,
    headerText,
    formStyles,
    labelStyle,
    fieldStyle,
    selectStyle,
    autocompleteStyles,
    buttonContainer,
    buttonStyle,
    loaderStyles,
  } = residencyProgramSearchStyles;

  const {
    user,
    specialty: specialty,
    state,
    PGY,
    loading,
    institution,
    userOptions,
    institutionOptions,
    specialtyOptions: specialtyOptions,
    stateOptions,
    setUser,
    setSpecialty,
    setState,
    setPGY,
    handleSelectChange,
    handleInstitutionChange,
    PGYOptions,
    handleProceed,
  } = stateUtils();
  return (
    <Box sx={mainContainer}>
      <Select
        displayEmpty
        sx={userSelectStyle}
        value={user}
        onChange={(e) => setUser(e.target.value)}>
        <MenuItem disabled value=''>
          <em>Select User</em>
        </MenuItem>
        {userOptions.map((el: UserOptionsType) => (
          <MenuItem value={el.id}>{el.username}</MenuItem>
        ))}
      </Select>
      <Box sx={subContainer}>
        <Box sx={headerStyles}>
          <Typography sx={headerText}>Specify Your Current Program</Typography>
        </Box>
        <Box sx={formStyles}>
          <Box sx={fieldStyle}>
            <Typography sx={labelStyle}>Specialty:</Typography>
            <RPSelect
              sx={selectStyle}
              value={specialty}
              placeholder='Select Specialty Program'
              handleChange={(e) => handleSelectChange(e, setSpecialty)}
              options={specialtyOptions}
            />
          </Box>
          <Box sx={fieldStyle}>
            <Typography sx={labelStyle}>State:</Typography>
            <RPSelect
              sx={selectStyle}
              value={state}
              placeholder='Select State'
              handleChange={(e) => handleSelectChange(e, setState)}
              options={stateOptions}
            />
          </Box>
          <Box sx={fieldStyle}>
            <Typography sx={labelStyle}>PGY:</Typography>
            <RPSelect
              sx={selectStyle}
              value={PGY}
              placeholder='Select PGY'
              handleChange={(e) => handleSelectChange(e, setPGY)}
              options={PGYOptions}
            />
          </Box>
          <Box>
            <Typography sx={labelStyle}>
              Institution Name (omit, or include to limit selections) :
            </Typography>
            <Autocomplete
              disablePortal
              id='institutionList'
              value={institution}
              onChange={(e, value) => handleInstitutionChange(e, value)}
              options={institutionOptions}
              sx={autocompleteStyles}
              renderInput={(params) => (
                <TextField {...params} placeholder='Enter Institution Name' />
              )}
            />
          </Box>
          <Box sx={buttonContainer}>
            <Button
              variant='contained'
              onClick={handleProceed}
              sx={buttonStyle}
              disabled={!user.length}>
              Proceed
            </Button>
          </Box>
        </Box>
      </Box>
      {loading && (
        <Box sx={loaderStyles}>
          <CircularProgress />
        </Box>
      )}
    </Box>
  );
};

export default ResidencyProgramSearch;
