import landingImg from 'public/landingImg.jpg';

export const residencyProgramSearchStyles = {
  mainContainer: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    flexDirection:'column',
    alignItems: 'center',
    border: '0.5px solid #b2b2f5 ',
    width: '100%',
    backgroundImage: `url(${landingImg.src})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100% 100%',
    backgroundPosition: 'Center',
    borderRadius: '0.5rem',
  },
  userSelectStyle:{
    marginBottom: '1rem',
    backgroundColor: 'white',
    width: '30rem',
  },
  subContainer: {
    width: '40rem',
    border: '1px solid #573b8a',
    borderRadius: '0.5rem',
  },
  headerStyles: {
    borderBottom: '1px solid',
    background:
      'linear-gradient(45deg,rgb(166 108 238 / 75%) 0,rgb(95 128 236 / 75%) 50%,rgb(214 89 229 / 75%) 100%)',
    display: 'flex',
    justifyContent: 'center',
    color: 'white',
    padding: '1.5rem',
  },
  headerText: { fontWeight: '700', fontSize: '1.2rem' },
  formStyles: {
    padding: '3rem',
    paddingRight: '4.5rem',
    backgroundColor: 'white',
  },
  labelStyle: {
    color: '#573b8a',
    fontWeight: '600',
    alignSelf: 'center',
  },
  fieldStyle: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1rem',
  },
  selectStyle: { width: '20rem' },
  autocompleteStyles: {
    width: '30rem',
    marginTop: '1rem',
    '.MuiAutocomplete-endAdornment': {
      display: 'flex',
      top: 'auto',
    },
  },
  buttonContainer: { display: 'flex', justifyContent: 'center' },
  buttonStyle: {
    background:
      'linear-gradient(45deg,rgb(166 108 238 / 75%) 0,rgb(95 128 236 / 75%) 50%,rgb(214 89 229 / 75%) 100%)',
    fontSize: '1rem',
    fontWeight: '600',
    marginTop: '1rem',
    borderRadius: '0.5rem',
  },
  loaderStyles:{
    display: 'flex',
    position: 'fixed',
    bottom: '50%',
    width: '100%',
    justifyContent: 'center',
  }
};
