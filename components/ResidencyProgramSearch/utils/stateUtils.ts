import { BaseSyntheticEvent, SetStateAction, useEffect, useState } from 'react';
import { SelectChangeEvent } from '@mui/material';
import {
  InstitutionsService,
  ProfilesService,
  UserService,
} from '@/core/services';
import { camelCase } from '@/utils/functions/helperFunctions';
import { stateOptions, specialtyOptions, PGYOptions } from './configUtils';

interface GetAllInstitutionListType {
  id: number;
  title: string;
  pgy: string;
  specialties: string;
  location: string;
}
interface InstitutionValueType {
  label: string;
  id: string | number;
}
export interface UserOptionsType {
  username: string;
  id: string | number;
}

const HandleStateUtils = () => {
  const [userOptions, setUserOptions] = useState([]);
  const [institutionOptions, setInstitutionOptions] = useState<
    InstitutionValueType[]
  >([]);
  const [user, setUser] = useState('');
  const [specialty, setSpecialty] = useState('');
  const [state, setState] = useState('');
  const [PGY, setPGY] = useState('');
  const [institution, setInstitution] = useState({ label: '', id: '' });
  const [loading, setLoading] = useState(false);

  useEffect(() => {

    (async () => {
      setLoading(true);
      await UserService.getAllUsersList()
        .then((resp) => {
          setUserOptions(resp.data.users);
        })
        .then(() => setLoading(false))
        .catch((err) => {
          setLoading(false);
          return err;
        });
    })();

    (async () => {
      setLoading(true);
      await InstitutionsService.getAllInstitutionList()
        .then((resp) => {
          resp.data.institutionsEntries.map(
            (item: GetAllInstitutionListType) => {
              setInstitutionOptions((prev) =>
                !prev.includes({ label: item.title, id: item.id })
                  ? [...prev, { label: item.title, id: item.id }]
                  : [...prev]
              );
            }
          );
        })
        .then(() => setLoading(false))
        .catch((err) => {
          setLoading(false);
          return err;
        });
    })();
  }, []);

  const handleSelectChange = (event: SelectChangeEvent, setter: any) => {
    setter(event.target.value);
  };

  const handleInstitutionChange = (
    e: BaseSyntheticEvent,
    value: InstitutionValueType | null
  ) => {
    setInstitution(value as SetStateAction<{ label: string; id: string; }>);
  };

  const handleProceed = () => {
    setLoading(true);
    ProfilesService.saveProfile({
      institution_name: institution.id,
      location: state,
      specialties: camelCase(specialty),
      user: user,
      title: institution.label,
      pgy: PGY.toLowerCase().replace(' ', ''),
    }).then(() => {
      setLoading(false);
      setSpecialty('');
      setState('');
      setPGY('');
      setInstitution({ label: '', id: '' });
    });
  };

  return {
    specialty: specialty,
    setSpecialty: setSpecialty,
    state,
    setState,
    loading,
    PGY,
    setPGY,
    stateOptions,
    institution,
    userOptions,
    user,
    setUser,
    institutionOptions,
    setInstitution,
    specialtyOptions: specialtyOptions,
    handleSelectChange,
    handleInstitutionChange,
    PGYOptions,
    handleProceed,
  };
};

export default HandleStateUtils;
