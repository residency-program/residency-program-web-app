import { gql } from '@apollo/client';
import { client } from '../client';

const API_URL =
  process.env.NEXT_PUBLIC_CMS_URL || 'https://my-craft-project.ddev.site/api';

const getAllInstitutionListQuery = gql`
  query Institutions {
    institutionsEntries {
      ... on institutions_default_Entry {
        id
        title
      }
    }
  }
`;

export const getAllInstitutionList = async () =>
  await client(API_URL).query({
    query: getAllInstitutionListQuery,
  });
