import { gql } from '@apollo/client';
import { client } from '../client';

const API_URL =
  process.env.NEXT_PUBLIC_CMS_URL || 'https://my-craft-project.ddev.site/api';

const getAllUsersQuery = gql`
  query MyQuery {
    users {
      email
      id
      username
    }
  }
`;

export const getAllUsersList = async () =>
  await client(API_URL).query({
    query: getAllUsersQuery,
  });
