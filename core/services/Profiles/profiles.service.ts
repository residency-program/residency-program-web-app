import { gql } from '@apollo/client';
import { client } from '../client';

const API_URL =
  process.env.NEXT_PUBLIC_CMS_URL || 'https://my-craft-project.ddev.site/api';

const saveProfileMutation = gql`
  mutation saveProfile(
    $institution_name: [Int]
    $location: String
    $specialties: String
    $authorId: ID
    $title: String
    $pgy: String
    
  ) {
    save_profiles_default_Entry(
      institution_name: $institution_name
      location: $location
      specialties: $specialties
      pgy: $pgy
      title: $title
      authorId: $authorId
    ) {
      id
    }
  }
`;

export const saveProfile = async (input: any) => {
  await client(API_URL).mutate({
    mutation: saveProfileMutation,
    variables: {
      institution_name: input.institution_name*1,
      location: input.location,
      specialties: input.specialties,
      pgy: input.pgy,
      authorId: input.user*1,
      title: input.title,
    },
  });
};
