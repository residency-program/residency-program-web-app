import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';

export const client = (apiUrl: string) =>
  new ApolloClient({
    link: createHttpLink({ uri: apiUrl }),
    cache: new InMemoryCache(),
  });
