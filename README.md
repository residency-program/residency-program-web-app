# residency-program-web-app
In this repository we're creating a service to showcase the data through craft CMS application.


## Installing packages

Open the CLI and run command:

`npm install`

## Getting Started

Run the development server:

```bash
`npm run dev`
# or
`yarn dev`
# or
`pnpm dev`
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the Login Page.

Go to [http://localhost:3000/dashboard](http://localhost:3000/dashboard) to access the residency program form.