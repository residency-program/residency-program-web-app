export const camelCase = (str: String) => {
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word: string, index) {
      return index == 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, '');
};

export const emailRegexp = '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}';
