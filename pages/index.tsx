import ResidencyProgramSearch from '@/components/ResidencyProgramSearch';

export default function Home() {
  return (
    <>
      <ResidencyProgramSearch />
    </>
  );
}
